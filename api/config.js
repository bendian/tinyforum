module.exports = {
    ENV: process.env.NODE_ENV || 'developement',
    PORT: process.env.PORT || 8080,
    URL: process.env.BASE_URL || 'http://localhost:8080',
    MONGODB_URI: process.env.MONGODB_URI || 'mongodb://127.0.0.1:27017'
}