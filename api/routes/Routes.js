const jwt = require('jsonwebtoken');
const tokenisation = require('../tokenisation.js');
const posts_controller = require('../controllers/Controllers.js');
const login_controller = require('../controllers/loginController.js');
module.exports = app => {

	const verify = (req, res, next) => {
		var token = req.query.token;
		jwt.verify(token, 'supersecret', function (err, decoded) {
			if (!err) {
				next();
			} else {
				res.sendStatus(401);
			}
		});
	}

	app.use('/posts', verify);
	app.route('/')
		.get((req, res) => {
			res.send("Welcome to my api");
		});

	app.route('/token')
		.get(tokenisation.get_token);

	app.route('/login/:userName')
		.get(login_controller.check_username)
		.post(login_controller.checkin_username)
		.delete(login_controller.checkout_username);

	app.route('/posts')
		.post(posts_controller.create_post)
		.get(posts_controller.get_all_posts);

	app.route('/posts/:postId')
		.get(posts_controller.get_post)
		.delete(posts_controller.delete_post);

	app.route('/posts/:postId/meta/comments')
		.post(posts_controller.comment_post)
		.delete(posts_controller.delete_comment);

	app.route('/posts/:postId/meta/likes')
		.post(posts_controller.likes_inc)
		.delete(posts_controller.likes_dec);

	app.route('/posts/:postId/meta/dislikes')
		.post(posts_controller.dislikes_inc)
		.delete(posts_controller.dislikes_dec);
}
